# About Medium Blog Articles

_This module allows users to import Medium articles from any Publication available at http://medium.com/_

_It uses a publication's link in order to generate the specified number of articles desired._

How to use it?

_1. Enable the Medium Blog Articles module_

_2. Go to the module's configuration page, found on the configuration page under the Development category: 
https://screencast.com/t/eiULxI5ZprG , https://screencast.com/t/n1SXNbGsSh ._

_3. Place your block in the desired region of your theme from the block structure found at: admin/structure/block_

_4. Run CRON manually._

_5. Clear caches, access the page wth the block and refresh._

Maintainer

* Madalina Cotumbeanu (https://www.drupal.org/u/maddiec)

Supporting organization

* Softescu SRL (https://www.drupal.org/softescu-srl)
